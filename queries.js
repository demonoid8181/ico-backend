/* eslint-disable no-template-curly-in-string */
var sha3 = require('js-sha3')
var promise = require('bluebird')
const nodemailer = require('nodemailer')
const generatePassword = require('password-generator')

const sha256 = sha3.sha3_256

const options = {
    // Initialization Options
    promiseLib: promise
}

const smtpTransport = nodemailer.createTransport({
    service: 'Yandex',
    auth: {
        user: 'noreply@amastar.net',
        pass: 'tlh4^E*1:R`w'
    }
})

const pgp = require('pg-promise')(options)
const connectionString = 'postgres://amastar:pAsSw0rD28085234@213.169.149.109:5432/amastar'
const db = pgp(connectionString)

// add query functions

module.exports = {
    getAllUsers: getAllUsers,
    getSingleUser: getSingleUser,
    createUser: createUser,
    updateUser: updateUser,
    removeUser: removeUser,
    getAllSubs: getAllSubs,
    createSub: createSub,
    removeSub: removeSub,
    registerUser: registerUser,
    findUser: findUser,
    findsub: findsub,
    login: login,
    sendVerify: sendVerify,
    verify: verify,
    sendPass: sendPass,
    verifyPass: verifyPass
}

function getAllUsers (req, res, next) {
    db.any('select * from users')
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Retrieved ALL users'
                })
        })
        .catch(function (err) {
            return next(err)
        })
}

function getAllSubs (req, res, next) {
    db.any('select * from subs')
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Retrieved ALL subs'
                })
        })
        .catch(function (err) {
            return next(err)
        })
}

function getSingleUser (req, res, next) {
    var pupID = parseInt(req.params.id)
    db.one('select * from users where id = $1', pupID)
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Retrieved ONE user'
                })
        })
        .catch(function (err) {
            return next(err)
        })
}

function findUser (req, res, next) {
    db.any('select * from users where email = $1', req.query.email)
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Retrieved all user with request email'
                })
        })
        .catch(function (err) {
            return next(err)
        })
}

function login (req, res, next) {
    console.log(req.query)
    db.any('select * from users where verify=1 and email = ${email} and password = ${pass}', req.query)
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Retrieved all user with request email and password'
                })
        })
        .catch(function (err) {
            return next(err)
        })
}

function createUser (req, res, next) {
    req.body.amount = parseInt(req.body.amount)
    db.none('insert into users(name, email, country, amount, eth, password)' +
        'values(${name}, ${email}, ${country}, ${amount},  ${eth}, ${password})',
    req.body)
        .then(function () {
            res.status(200)
                .json({
                    status: 'success',
                    message: 'Inserted one user'
                })
        })
        .catch(function (err) {
            return next(err)
        })
}

function registerUser (req, res, next) {
    db.none('insert into users(email, password)' +
        'values(${email}, ${password})',
    req.body)
        .then(function () {
            res.status(200)
                .json({
                    status: 'success',
                    message: 'register one user'
                })
        })
        .catch(function (err) {
            return next(err)
        })
}

function updateUser (req, res, next) {
    console.log(req.body)
    db.none('update users set name=$1, country=$3, amount=$4, eth=$5, password=$6 where email=$2',
        [req.body.params.name, req.body.params.email, req.body.params.country, parseInt(req.body.params.amount),
            req.body.params.adress, req.body.params.password])
        .then(function () {
            res.status(200)
                .json({
                    status: 'success',
                    message: 'Updated user'
                })
        })
        .catch(function (err) {
            return next(err)
        })
}

function removeUser (req, res, next) {
    var userID = parseInt(req.query.id)
    db.result('delete from users where id = $1', userID)
        .then(function (result) {
            /* jshint ignore:start */
            res.status(200)
                .json({
                    status: 'success',
                    message: `Removed ${result.rowCount} user`
                })
            /* jshint ignore:end */
        })
        .catch(function (err) {
            return next(err)
        })
}

function removeSub (req, res, next) {
    var subID = parseInt(req.query.id)
    db.result('delete from subs where id = $1', subID)
        .then(function (result) {
            /* jshint ignore:start */
            res.status(200)
                .json({
                    status: 'success',
                    message: `Removed ${result.rowCount} sub`
                })
            /* jshint ignore:end */
        })
        .catch(function (err) {
            return next(err)
        })
}

function createSub (req, res, next) {
    db.none('insert into subs (email) values(${email})',
        req.body)
        .then(function () {
            res.status(200)
                .json({
                    status: 'success',
                    message: 'Inserted one sub'
                })
        })
        .catch(function (err) {
            return next(err)
        })
}

function findsub (req, res, next) {
    console.log(req.query)
    db.any('select * from subs where email = $1', req.query.email)
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Retrieved all subs with request email'
                })
        })
        .catch(function (err) {
            return next(err)
        })
}

// Отправка письма для верификации
function sendVerify (req, res, next) {
    const rand = sha256(Math.random().toString(36).substr(2, 36))
    const link = 'https://' + req.get('host') + ':3000/api/verify?id=' + rand 
    const mailOptions = {
        to: req.query.email,
        from: 'noreply@amastar.net',
        subject: 'Please confirm your Email account',
        html: 'Hello,<br> Please Click on the link to verify your email.<br><a href=' + link + '>Click here to verify</a>'
    }
    console.log(mailOptions)
    smtpTransport.sendMail(mailOptions, (error, response) => {
        if (error) {
            console.log(error)
            res.end('error')
        } else {
            console.log('Message sent: ' + response.messageId)
        }
    })
    db.none('update users set verifykey=$1 where email=$2', [rand, req.query.email])
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Retrieved verify email'
                })
        })
        .catch(function (err) {
            return next(err)
        })
}

// верификация пользователя
function verify (req, res, next) {
    console.log(req.protocol + ':/' + req.get('host'))
    if ((req.protocol + '://' + req.get('host')) === ('http://amastar.net')) {
        console.log('Domain is matched. Information is from Authentic email')
	console.log(req.query.id)
        db.any('select * from users where verifykey = ${id}', req.query)
            .then(function (data) {
                 console.log(data)
		 if (data.length > 0) {
                     console.log(data[0].email)
		     db.none('update users set verify=1 where email=$1 and verifykey = $2', [data[0].email,data[0].verifykey] )
                        .then(function (data) {
                            res.status(301).redirect('https://' +  req.get('host') + '/verify')
                        })
                        .catch(function (err) {
                            res.status(301).redirect('https://' + req.get('host') + '/wrong')
                        })
		}
            })
            .catch(function (err) {
                 res.status(301).redirect('https://' + req.get('host') + '/wrong')
            })
    } else {
        res.status(301).redirect('https://' + req.get('host') + '/wrong')
    }
}

// Отправка письма для верификации
function verifyPass (req, res, next) {
    const rand = sha256(Math.random().toString(36).substr(2, 36))
    const link = 'https://' + req.get('host') + ':3000/api/sendPass?id=' + rand + '&email=' + req.query.email
    const mailOptions = {
        to: req.query.email,
        from: 'noreply@amastar.net',
        subject: 'Please confirm your Email account for send you password',
        html: 'Hello,<br> Please Click on the link to verify your email to send your new password.<br><a href=' + link + '>Click here to verify</a>'
    }
    console.log(mailOptions)
    db.none('update users set verifykey=$1 where email=$2', [rand, req.query.email])
        .then(function (data) {
            smtpTransport.sendMail(mailOptions, (error, response) => {
               if (error) {
                    console.log(error)
                    res.end('error')
               } else {
                    console.log('Message sent: ' + response.messageId)

            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Retrieved verify email'
                })

               }
           })

        })
        .catch(function (err) {
            return next(err)
        })
}

// Отправка письма для верификации пароля
function sendPass (req, res, next) {
    const pass = generatePassword(12, false)
    const mailOptions = {
        to: req.query.email,
        from: 'noreply@amastar.net',
        subject: 'New password',
        html: 'Hello,<br> This is your new password<br>' +
            '<h3>' + pass + '</h3>' +
            '<br>Do not show it to anyone.'
    }
    console.log(mailOptions)
    if (req.query.id !== '') {
    db.none('update users set password=$3 where email=$2 and verifykey=$1', [req.query.id, req.query.email, pass])
        .then(function (data) {
            smtpTransport.sendMail(mailOptions, (error, response) => {
                if (error) {
                    console.log(error)
                    res.end('error')
                } else {
                    console.log('Message sent: ' + response.messageId)
                }
            })
        })
        .catch(function (err) {
            return next(err)
        })
    }
}
