const express = require('express');
const router = express.Router();

const db = require('../queries');

router.get('/api/users', db.getAllUsers);
router.get('/api/user/:id', db.getSingleUser);
router.post('/api/user', db.createUser);
router.put('/api/user', db.updateUser);
router.delete('/api/user', db.removeUser);
router.post('/api/register', db.registerUser);
router.get('/api/findUser', db.findUser);
router.get('/api/login', db.login);

router.get('/api/subs', db.getAllSubs);
router.post('/api/sub', db.createSub);
router.delete('/api/sub', db.removeSub);
router.get('/api/findsub', db.findsub);

router.get('/api/sendVerify', db.sendVerify);
router.get('/api/verify', db.verify);

router.get('/api/sendPass', db.sendPass)
router.get('/api/verifyPass', db.verifyPass)

module.exports = router;
